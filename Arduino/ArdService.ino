#include <Servo.h> 

int maxPin=13;
int maxADC=6;
char* data;



void setup()
{
	Serial.begin(115200);
}

void loop()
{
	if(Serial.available())
	{
		data=readCommand();
                Serial.print(data);
	}
}

int performAction(int AT, int* params)
{
	switch(AT)
	{
		case 0: 
			return digitalOut(params);
		case 1:
			return digitalIn(params);
		case 2:
			return voltajeRead(params);
		case 3:
			return serPosition(params);
		case 4:
			return PWM(params);
		case 5:
			return toneGen(params);
		default:
			return -1; 
	}
}

char* readCommand()
{
	char* inputBuffer = malloc(sizeof(char) * 10);
	int len;
	int AT;
	if(Serial.find("~"))
	{
		delay(20);
		len=Serial.read();
		Serial.readBytes(inputBuffer,len);
		return inputBuffer;
	}
	else
	{
		return "Error";
	}
}

int getAT(char* data)
{
	char* ATcommands[] = {"DO","DI","VL","SR","PW","TN"};
	for(int i=0;i<=5;i++)
	{
		if((data[0]==ATcommands[i][0])&&(data[1]==ATcommands[i][1]))
		{
			return i;
		}
	}
	return -1;
}

int* getParam(char* data)
{
	int len=sizeof(data)/sizeof(char);
	int params[len-2];
	for(int i=0;i<(len-2);i++)
	{
		params[i]=data[i+2];
	}
	return params;
}

int digitalOut(int* params)
{
	pinMode(params[0],OUTPUT);
	if((params[0]<=maxPin) && (params[1]==0) || (params[1]==1))
	{
		digitalWrite(params[0],params[1]);
		return 0;
	}
	return -1;
}

int digitalIn(int* params)
{
	pinMode(params[0],INPUT);
	if(params[0]<=maxPin)
	{
		return digitalRead(params[0]);
	}
	return -1;
}

int voltajeRead(int* params)
{
	int ADCs[]={A0,A1,A2,A3,A4,A5};
	if(params[0]<=maxADC)
	{
		return analogRead(ADCs[params[0]]);
	}
	return -1;
}

int serPosition(int* params)
{
	if((params[0]<=maxPin)&&(params[1]<=180))
	{
		Servo servo;
		servo.attach(params[0]);
		servo.write(params[1]);
		delay(3000);
		servo.detach();
		delete &servo;
		return 0;
	}
	return -1;
}

int PWM(int* params)
{
	int pwmPins[]={3,5,6,9,10,11};
	int len=sizeof(pwmPins)/sizeof(int);
	for (int i=0;i<len;i++)
	{
		if(params[0]==pwmPins[i])
		{
			analogWrite(params[0],params[1]);
			return 0;
		}
	}
	return -1;
}

int toneGen(int* params)
{
	if((params[0]<=maxPin)&&(params[1]<100)&&(params[2]<100))
	{
		int frec=params[1]*1000+params[2]*10;
		int duration=params[3]*1000;
		tone(params[0],frec,duration);
		delay(duration);
		return 0;
	}
	return -1;
}
