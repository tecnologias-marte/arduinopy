#ifndef PYDUINO_H_
#define PYDUINO_H_

#include <Arduino.h>
#include "aJSON.h"

class Arduino{
	public:
		Arduino(int baud);
		void run();
		void setup();
		void readCommand();
		int  analogPinRead(int pin);
		bool digitalPinRead(int pin);
		bool pwmPinWrite(int pin, int val);
		bool digitalPinWrite(int pin, bool val);

	protected:
		void parseCommand(String data);
		int baud;
};

class Mega : public Arduino{
	public:
		Mega(int baud);
};

class Uno : public Arduino{
	public:
		Uno(int baud);
};

#endif
