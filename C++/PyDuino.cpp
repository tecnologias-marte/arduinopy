#include <Arduino.h>
#include "PyDuino.h"
#include "aJSON.h"

Arduino::Arduino(int baud){
    this->baud = baud;
}

void Arduino::setup(){
    Serial.begin(9600);
}

bool Arduino::digitalPinRead(int pin){
    pinMode(pin, INPUT);
    bool ans = digitalRead(pin);
    return ans;
}

bool Arduino::digitalPinWrite(int pin, bool val){
    pinMode(pin, OUTPUT);
    digitalWrite(pin, val);
    return true;
}

int Arduino::analogPinRead(int pin){
    int ans = analogRead(pin);
    return ans;
}

bool Arduino::pwmPinWrite(int pin, int val){
    analogWrite(pin, val);
    return true;
}

void Arduino::run(){

}

void Arduino::readCommand(){
    bool read = false;
    char inputBuff [250];
    memset (inputBuff,'\0',250);
    if(Serial.available() > 0 && Serial.find("{")){
        Serial.readBytes(inputBuff, 250);
        read = true;
    }

    if(read){
        String data = String("{");
        data += inputBuff;
        parseCommand(data);
    }
}

void Arduino::parseCommand(String data){
    char jsonString [data.length()];
    data.toCharArray(jsonString, data.length());
    aJsonObject* json = aJson.parse(jsonString);
    aJsonObject* cmdJson = aJson.getObjectItem(json , "cmd");

    String cmd = String(cmdJson->valuestring);

    if(cmd == "WD"){
        aJsonObject* port = aJson.getObjectItem(json , "port");
        aJsonObject* stat = aJson.getObjectItem(json , "stat");
        bool success = digitalPinWrite(port->valueint, stat->valueint);

        String json = String("{\"Err\":");
        json += !success;
        json += "}";

        Serial.println(json);
    }

    if(cmd == "RD"){
        aJsonObject* port = aJson.getObjectItem(json , "port");
        bool val = digitalPinRead(port->valueint);

        String json = String("{\"Stat\":");
        json += val;
        json += ", \"Err\":0}";

        Serial.println(json);
    }

    if(cmd == "AR"){
        aJsonObject* port = aJson.getObjectItem(json , "port");
        int val = analogPinRead(port->valueint);

        String json = String("{\"Stat\":");
        json += val;
        json += ", \"Err\":0}";

        Serial.println(json);
    }

    if(cmd == "WP"){
        aJsonObject* port = aJson.getObjectItem(json , "port");
        aJsonObject* stat = aJson.getObjectItem(json , "freq");
        bool success = pwmPinWrite(port->valueint, stat->valueint);

        String json = String("{\"Err\":");
        json += !success;
        json += "}";

        Serial.println(json);
    }
}

Mega::Mega(int baud) : Arduino(baud){}

Uno::Uno(int baud) :Arduino(baud){}

