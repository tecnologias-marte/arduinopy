from ArduinoMega import ArduinoMega
from ArduinoMega import ArduinoMegaADK
from ArduinoMega import ArduinoMega2560
from ArduinoUno import ArduinoUno


class ArduinoFactory(object):

    def __init__(self):
        pass

    def getAruinoMega(self,
                      autodetect=False,
                      serialPort='/dev/ttyACM0',
                      baudrate=9600,
                      name='ArduinoMega'):
        return ArduinoMega(autodetect, serialPort, baudrate, name)

    def getArduinoMegaADK(self,
                          autodetect=False,
                          serialPort='/dev/ttyACM0',
                          baudrate=9600,
                          name='ArduinoMegaADK'):
        return ArduinoMegaADK(autodetect, serialPort, baudrate, name)

    def getArduinoMega2560(self,
                           autodetect=False,
                           serialPort='/dev/ttyACM0',
                           baudrate=9600,
                           name='ArduinoMega2560'):
        return ArduinoMega2560(autodetect, serialPort, baudrate, name)

    def getArduinoUno(self,
                      autodetect=False,
                      serialPort='/dev/ttyACM0',
                      baudrate=9600,
                      name='ArduinoUno'):
        return ArduinoUno(autodetect, serialPort, baudrate, name)
