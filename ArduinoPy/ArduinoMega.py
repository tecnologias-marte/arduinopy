from Arduino import Arduino


class ArduinoMega(Arduino):
    def __init__(self,
                 autodetect=False,
                 serialPort='/dev/ttyACM0',
                 baudrate=9600,
                 name='ArduinoMega'):

        Arduino.__init_(self, autodetect, serialPort, baudrate, name)
        self._PWM = range(2, 14) + range(44, 47)
        self._digitalPorts = range(55)
        self._analogInput = range(16)


class ArduinoMegaADK(ArduinoMega):
    def __init__(self,
                 autodetect=False,
                 serialPort='/dev/ttyACM0',
                 baudrate=9600,
                 name='ArduinoMegaADK'):

        ArduinoMega.__init__(self, autodetect, serialPort, baudrate, name)


class ArduinoMega2560(ArduinoMega):
    def __init__(self,
                 autodetect=False,
                 serialPort='/dev/ttyACM0',
                 baudrate=9600,
                 name='ArduinoMega2560'):

        ArduinoMega.__init__(self, autodetect, serialPort, baudrate, name)
