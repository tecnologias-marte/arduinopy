import json
import serial
#rom serial import Serial
from serial import SerialException


class ArduinoException(Exception):
    pass


class Arduino(object):

    def __init__(self,
                 autodetect=False,
                 SerialPort='/dev/ttyACM0',
                 baudrate=9600,
                 name='Arduino'):
        try:
            self.serial = serial.Serial('/dev/ttyACM0', baudrate=9600, timeout=10)
        except SerialException:
            raise ArduinoException("Device not found!")

        self._PWM = []
        self._digitalPorts = []
        self._analogInput = []

        self.name = name

    def _sendData(self, data):
        jsonString = json.dumps(data)
        self.serial.write(jsonString)

    def _getData(self):
        buff = ""
        #print self.serial
        while True:
            buff = self.serial.read(self.serial.inWaiting())
            if buff:
                print buff

    def writeDigitalPort(self, port, status):
        data = {'cmd': 'WD', 'port': port, 'stat': status}
        badPort = not data['port'] in self._digitalPorts
        if badPort:
            raise ArduinoException("Wrong port for digital operation")

        self._sendData(data=data)
        jsonAns = self._getData()
        if jsonAns['Err'] == 1:
            raise ArduinoException("Internal error")
            return False

        return True

    def readDigitalPort(self, port):
        data = {'cmd': 'RD', 'port': port}
        badPort = not data['port'] in self._digitalPorts
        if badPort:
            raise ArduinoException("Wrong port for digital operation")

        self._sendData(data=data)
        jsonAns = self._getData()
        if jsonAns['Err'] == 1:
            raise ArduinoException("Internal error")

        return jsonAns['Stat']

    def readAnalogRead(self, port):
        data = {'cmd': 'AR', 'port': port}
        badPort = not data['port'] in self._analogInputs
        if badPort:
            raise ArduinoException("Wrong port for analog read")

        self._sendData(data=data)
        jsonAns = self._getData()
        if jsonAns['Err'] == 1:
            raise ArduinoException("Internal error")

        return jsonAns['Stat']

    def writePWM(self, port, freq):
        data = {'cmd': 'WP', 'port': port, 'freq': freq}
        badFreq = (data['freq'] > 1023 or data['freq'] < 0)
        if badFreq:
            raise ArduinoException("Worng frerq for PWM")

        badPort = not data['port'] in self._PWM
        if badPort:
            raise ArduinoException("Wrong port for PWM operation")

        self._sendData(data=data)
        jsonAns = self._getData()
        if jsonAns['Err'] == 1:
            raise ArduinoException("Internal error")
            return False

        return True
