from Arduino import Arduino


class ArduinoUno(Arduino):
    def __init__(self,
                 autodetect=False,
                 serialPort='/dev/ttyACM0',
                 baudrate=9600,
                 name='ArduinoMegaADK'):

        Arduino.__init__(self, autodetect, serialPort, baudrate, name)
        self._PWM = [3, 5, 6, 9, 10]
        self._digitalPorts = range(14)
        self._analogInput = range(6)
